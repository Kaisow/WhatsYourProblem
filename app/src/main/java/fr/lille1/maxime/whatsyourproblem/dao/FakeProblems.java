package fr.lille1.maxime.whatsyourproblem.dao;

import java.util.ArrayList;
import java.util.List;

import fr.lille1.maxime.whatsyourproblem.R;
import fr.lille1.maxime.whatsyourproblem.model.Problem;

/**
 * Created by maxime on 07/12/2017.
 * Cette classe contient un ensemble de faux problèmes
 */

class FakeProblems {
    private List<Problem> problems = null;

    FakeProblems() {
        problems = new ArrayList<>();

        Problem problem = new Problem();

        problem.setLatitude("0.456253657");
        problem.setLongitude("0.37452736437");
        problem.setProblemType(R.string.autre);
        problem.setDescription("Vive RadioKing");
        problem.setAddress("Chez Gaston");

        problems.add(problem);

        problem = new Problem();

        problem.setLatitude("60.456253657");
        problem.setLongitude("3.37452736437");
        problem.setProblemType(R.string.haie_tailler);
        problem.setDescription("Ceci est une autre description");
        problem.setAddress("Chez Paul");

        problems.add(problem);

        problem = new Problem();

        problem.setLatitude("0.456253657");
        problem.setLongitude("50.37452736437");
        problem.setProblemType(R.string.detritus);
        problem.setDescription("Ceci est une description");
        problem.setAddress("Chez fabrice");

        problems.add(problem);

        problem = new Problem();

        problem.setLatitude("0.456253657");
        problem.setLongitude("50.37452736437");
        problem.setProblemType(R.string.arbre_abattre);
        problem.setDescription("Toujours une autre description");
        problem.setAddress("Je ne sais pas où");

        problems.add(problem);

        problem = new Problem();

        problem.setLatitude("10.456253657");
        problem.setLongitude("13.37452736437");
        problem.setProblemType(R.string.arbre_tailler);
        problem.setDescription("Ca prend du temps de changer les données...");
        problem.setAddress("Quelque part sur terre");

        problems.add(problem);

        problem = new Problem();

        problem.setLatitude("0.456253657");
        problem.setLongitude("50.37452736437");
        problem.setProblemType(R.string.haie_tailler);
        problem.setDescription("Je ne pense pas qu'il y ait de bonne où de mauvaise situation");
        problem.setAddress("Quelque part en random");

        problems.add(problem);

        problem = new Problem();

        problem.setLatitude("50.1231213");
        problem.setLongitude("50.1767342");
        problem.setProblemType(R.string.autre);
        problem.setDescription("J'arrête de changer les descriptions après celui-là");
        problem.setAddress("Dans l'espace");

        problems.add(problem);

        problem = new Problem();

        problem.setLatitude("0.456253657");
        problem.setLongitude("50.37452736437");
        problem.setProblemType(R.string.detritus);
        problem.setDescription("Ceci est une description");
        problem.setAddress("Chez fabrice");

        problems.add(problem);

        problem = new Problem();

        problem.setLatitude("0.456253657");
        problem.setLongitude("50.37452736437");
        problem.setProblemType(R.string.arbre_abattre);
        problem.setDescription("Ceci est une description");
        problem.setAddress("Bonne questions, lancer la map pour voir");

        problems.add(problem);

        problem = new Problem();

        problem.setLatitude("0.425546251");
        problem.setLongitude("2.113424");
        problem.setProblemType(R.string.arbre_tailler);
        problem.setDescription("Ceci est une description");
        problem.setAddress("En terre inconnue");

        problems.add(problem);
    }

    List<Problem> getFakeList() {
        return problems;
    }
}
