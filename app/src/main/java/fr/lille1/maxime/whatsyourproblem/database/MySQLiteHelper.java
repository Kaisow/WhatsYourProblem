package fr.lille1.maxime.whatsyourproblem.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by maxime on 26/11/2017.
 * Cette classe est le coeur de gestion de la base de données
 * Elle gère sa création et sa mise à jour
 */

public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_PROBLEM = "problem";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_PROBLEM_TYPE = "probleme_type";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_DESCRIPTION = "description";

    private static final String DATABASE_NAME = "problems.db";
    private static final int DATABASE_VERSION = 1;

    // SQL command to create the table
    private static final String DATABASE_CREATE =
            "create table " + TABLE_PROBLEM + "(" +
            COLUMN_ID + " integer primary key autoincrement, " +
            COLUMN_PROBLEM_TYPE + " text not null, " +
            COLUMN_LATITUDE + " text not null, " +
            COLUMN_LONGITUDE + " text not null, " +
            COLUMN_ADDRESS + " text not null, " +
            COLUMN_DESCRIPTION + " text);";

    // SQL command to remove the table
    private static final String DATABASE_DELETE = "DROP TABLE IF EXISTS " + TABLE_PROBLEM;

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DATABASE_DELETE);
        onCreate(db);
    }
}
