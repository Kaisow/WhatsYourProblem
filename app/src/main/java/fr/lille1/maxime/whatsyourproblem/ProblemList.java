package fr.lille1.maxime.whatsyourproblem;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import fr.lille1.maxime.whatsyourproblem.adapter.ProblemAdapter;
import fr.lille1.maxime.whatsyourproblem.dao.ProblemDAO;
import fr.lille1.maxime.whatsyourproblem.manager.DialogManager;
import fr.lille1.maxime.whatsyourproblem.manager.SnackbarManager;
import fr.lille1.maxime.whatsyourproblem.model.Problem;

/**
 * Cette activitié est utilisée pour lister les différents problèmes
 */
public class ProblemList extends AppCompatActivity {

    private final Context _this = this;
    private List<Problem> problems;
    private ProblemDAO problemDao = new ProblemDAO(this);
    private ListView problemList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problem_list);
        problemList = findViewById(R.id.problem_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listAllProblems();

        // On gère l'action de click sur le bouton d'ajout
        FloatingActionButton add_button = findViewById(R.id.add_button);
        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(_this, ProblemForm.class);
                startActivityForResult(intent, 1);
            }
        });

        // On gère l'action de click sur un élément de la liste
        problemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                loadProblemWithId(position);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_problem_list, menu);
        return true;
    }

    /**
     * Cette méthode permet de gerer l'affichage des problèmes
     * Elle est également utiliser pour rafraîchir la liste des problèmes
     */
    private void listAllProblems() {

        problemDao.open();
        problems = problemDao.getAllProblems();

        problemDao.close();

        ProblemAdapter adapter = new ProblemAdapter(this, problems);

        problemList.setAdapter(adapter);

    }

    /**
     * Cette fonction lance la vue des détails avec le problème associé
     * Le problème associé est une corrélation entre la position de l'élément cliqué dans la liste
     * et la position du problème en question
     * @param pos position de l'élément ou le clic a été effectué
     */
    private void loadProblemWithId(int pos) {
        Intent intent = new Intent(_this, ProblemDetails.class);
        intent.putExtra("problem_id", problems.get(pos).getId());
        startActivityForResult(intent, 2);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // On récupère l'id de l'élément du menu sur lequel on a cliqué
        int id = item.getItemId();

        // S'il s'agit du bonton d'ajout de faux problèmes
        if (id == R.id.faux_problems) {
            problemDao.addFakeProblem();
            CoordinatorLayout coordinatorLayout = findViewById(R.id.problem_list_coordinator_layout);
            SnackbarManager.showSnackbar(_this, coordinatorLayout, getString(R.string.fixtures_ajoutes), getString(R.string.ok), Snackbar.LENGTH_LONG);
            listAllProblems();
        }
        // S'il s'agit du bouton de suppression
        else if(id == R.id.delete_all) {
            DialogManager.showDescriptionDialog(
                    getString(R.string.tout_supprimer_confirmation),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    problemDao.purgeAllProblem();
                                    CoordinatorLayout coordinatorLayout = findViewById(R.id.problem_list_coordinator_layout);
                                    SnackbarManager.showSnackbar(_this, coordinatorLayout, getString(R.string.problemes_supprimes), getString(R.string.ok), Snackbar.LENGTH_LONG);
                                    listAllProblems();
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    Toast.makeText(_this, "Suppression annulée", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }
                    }, _this);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Permet de gérer le retour d'une activité
     * @param requestCode Le code de configuré lors du lancement de la nouvelle activité, permet d'identifier quelle activité se termind
     * @param resultCode Le resultat de l'activité, si elle s'est bien terminée où non.
     * @param data Les données renvoyées de l'activité terminée
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // On ne fait rien de particulier pour le moment, on rafraîchit juste la liste des problèmes
        CoordinatorLayout coordinatorLayout = findViewById(R.id.problem_list_coordinator_layout);
        if(requestCode == 1 && resultCode == Activity.RESULT_OK) {
            SnackbarManager.showSnackbar(this, coordinatorLayout, getString(R.string.problem_ajoute), getString(R.string.ok), Snackbar.LENGTH_LONG);
        }
        else if(requestCode == 2 && data != null && data.getStringExtra("last_action").equals("delete")) {
            SnackbarManager.showSnackbar(this, coordinatorLayout, getString(R.string.probleme_supprime), getString(R.string.ok), Snackbar.LENGTH_LONG);
        }
        listAllProblems();
    }
}
