package fr.lille1.maxime.whatsyourproblem.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.lille1.maxime.whatsyourproblem.R;
import fr.lille1.maxime.whatsyourproblem.model.Problem;

/**
 * Cette classe est utilisée pour gérer l'affiche d'un problème au sein d'une liste
 */
public class ProblemAdapter extends ArrayAdapter<Problem> {

    private Context context;

    public ProblemAdapter(Context context, List<Problem> objects) {
        super(context, 0, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.problem_list_item, parent, false);
        }

        PermissionViewHolder viewHolder = (PermissionViewHolder) convertView.getTag();

        if(viewHolder == null) {

            viewHolder = new PermissionViewHolder();
            viewHolder.problem_type_name = convertView.findViewById(R.id.problem_type_name);
            viewHolder.problem_address = convertView.findViewById(R.id.problem_address);
            convertView.setTag(viewHolder);
        }

        Problem problem = getItem(position);

        assert problem != null;
        viewHolder.problem_type_name.setText(context.getString(problem.getProblemType()));
        viewHolder.problem_address.setText(problem.getAddress());

        return convertView;
    }


    private class PermissionViewHolder {
        TextView problem_type_name;
        TextView problem_address;
    }
}
