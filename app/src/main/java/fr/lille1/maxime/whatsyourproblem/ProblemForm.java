package fr.lille1.maxime.whatsyourproblem;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import fr.lille1.maxime.whatsyourproblem.dao.ProblemDAO;
import fr.lille1.maxime.whatsyourproblem.manager.DialogManager;
import fr.lille1.maxime.whatsyourproblem.model.Problem;
import fr.lille1.maxime.whatsyourproblem.model.ProblemType;


/**
 * Cette activité contient le formulaire et la gestion des valeurs pour l'ajout d'un nouveau problème
 */
public class ProblemForm extends AppCompatActivity {

    // Facilite l'accès au contextes dans les classes anonymes implémentées
    private final Context _this = this;
    // Id utilisée pour identifier la décision du résultat GPS
    private static final int GPS_PERMISSIONS = 1;
    // Permet de savoir si l'utilisateur à demander pour la géolocatisation
    private boolean askforlocation = false;
    private LocationManager locationManager;
    private LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problem_form);

        Spinner problem_type = findViewById(R.id.problem_type);

        problem_type.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, generateTranslatedProblem()));

        Button add_problem_button = findViewById(R.id.add_problem_button);

        // On gère le clic sur le bouton d'ajout
        add_problem_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean hasError = false;
                ((EditText)findViewById(R.id.latitude)).setError(null);
                ((EditText)findViewById(R.id.longitude)).setError(null);
                ((EditText)findViewById(R.id.address)).setError(null);
                ((EditText)findViewById(R.id.description)).setError(null);


                // Récupération de toutes les valeurs
                Spinner problem_type = findViewById(R.id.problem_type);
                String latitude = ((EditText) findViewById(R.id.latitude)).getText().toString();
                String longitude = ((EditText) findViewById(R.id.longitude)).getText().toString();
                String address = ((EditText) findViewById(R.id.address)).getText().toString();
                String description = ((EditText) findViewById(R.id.description)).getText().toString();

                // Cette regex permet de vérifier la validité des valeurs flottantes
                Pattern float_pattern = Pattern.compile(getString(R.string.float_regex));

                // On gère chaque problème pour afficher une erreur personnalisée
                if(latitude.equals("") || !float_pattern.matcher(latitude).matches()) {
                    ((EditText)findViewById(R.id.latitude)).setError(getString(R.string.lat_long_error));
                    hasError = true;
                }

                if(longitude.equals("") || !float_pattern.matcher(longitude).matches()) {
                    ((EditText)findViewById(R.id.longitude)).setError(getString(R.string.lat_long_error));
                    hasError = true;
                }

                if(address.equals("")) {
                    ((EditText)findViewById(R.id.address)).setError(getString(R.string.empty_field_error));
                    hasError = true;
                }

                if(description.equals("")) {
                    ((EditText)findViewById(R.id.description)).setError(getString(R.string.empty_field_error));
                    hasError = true;
                }


                // Si il n'y a pas d'erreur, on enregistre
                if(!hasError) {
                    Problem problem = new Problem();
                    problem.setProblemType(ProblemType.values()[problem_type.getSelectedItemPosition()].getId());
                    problem.setLatitude(latitude);
                    problem.setLongitude(longitude);
                    problem.setAddress(address);
                    problem.setDescription(description);

                    ProblemDAO problemDAO = new ProblemDAO(_this);
                    problemDAO.open();
                    Problem result = problemDAO.addProblem(problem);
                    problemDAO.close();

                    if (result != null) {
                        Intent resultIntent = new Intent();
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                    }
                }
            }
        });

        // On gère le clic sur le bouton de localisation
        Button location_button = findViewById(R.id.location_button);

        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkAllGpsAutorizations()) {
                    // On précise pourquoi on a besoin du GPS
                    DialogManager.showDescriptionDialog(
                            "Le GPS est nécessaire afin de définir votre position",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            // Si l'utilisateur accepte, on demande la permission
                                            ActivityCompat.requestPermissions((AppCompatActivity) _this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, GPS_PERMISSIONS);
                                            break;
                                        case DialogInterface.BUTTON_NEGATIVE:
                                            Toast.makeText(_this, getString(R.string.gps_refuse), Toast.LENGTH_SHORT).show();
                                            break;
                                    }
                                }
                            }, _this);
                }
                else {
                    getLocation();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(locationManager != null && locationListener != null) {
            askforlocation = false;
            locationManager.removeUpdates(locationListener);
        }
    }

    /**
     * Raccourci pour vérifier si les permissions GPS sont accordées où non
     * @return true si elle sont acordées, false sinon
     */
    private boolean checkAllGpsAutorizations() {
        return ContextCompat.checkSelfPermission(_this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(_this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Cette fonction permet de traduires les id des ressources string.xml
     * renvoyé par l'énumération des différents type de problèmes
     * @return La liste des problèmes avec leur "string" associée
     */
    private List<String> generateTranslatedProblem() {
        List<String> problemTypeString = new ArrayList<>();
        for (ProblemType pbType : ProblemType.values()) {
            problemTypeString.add(getString(pbType.getId()));
        }

        return problemTypeString;
    }

    /**
     * Permet de gérer la mise à jour de la localisation
     * @param location Variables contenant l'ensemble des coordonnées
     */
    @SuppressLint("SetTextI18n")
    private void makeUseOfNewLocation(Location location) {
        if (!askforlocation) return;

        // Attribution de la latitude et de la longitude
        EditText latitude = findViewById(R.id.latitude);
        EditText longitude = findViewById(R.id.longitude);
        latitude.setText(Double.toString(location.getLatitude()));
        longitude.setText(Double.toString(location.getLongitude()));

        getGPSAddress(location);

        findViewById(R.id.location_loading).setVisibility(View.INVISIBLE);
        // On arrête les mises à jour
        locationManager.removeUpdates(locationListener);
        askforlocation = false;
    }

    /**
     * Permet de gérer la demande par un utilisateur de la localisation
     */
    private void getLocation() {

        // Cette variable permet de savoir que c'est l'utilisateur qui a demander la localisation
        askforlocation = true;
        findViewById(R.id.location_loading).setVisibility(View.VISIBLE);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        assert locationManager != null;
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            findViewById(R.id.location_loading).setVisibility(View.INVISIBLE);
            DialogManager.showDescriptionDialog(
                    getString(R.string.gps_desactive),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    assert locationManager != null;
                                    if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 10);
                                    }
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    findViewById(R.id.location_loading).setVisibility(View.INVISIBLE);
                                    break;
                            }
                        }
                    }, _this);
            return;
        }

        locationListener = new LocationListener() {
            // On utilise uniquement la première méthode pour le moment
            public void onLocationChanged(Location location) {
                makeUseOfNewLocation(location);
            }

            // Non utilisé actuellement
            public void onStatusChanged(String provider, int status, Bundle extras) {}

            // Non utilisé actuellement
            public void onProviderEnabled(String provider) {}

            // Cela permet de savoir si le système de localisation est désactivé où non
            public void onProviderDisabled(String provider) {
            }
        };

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);

        // On ne fait rien dedans, la permission est gérée par le bouton, permet juste de ne pas avoir d'erreur
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if(locationManager != null) locationManager.requestSingleUpdate(criteria, locationListener, null);

    }

    /**
     * Permet d'auto-compléter le champ addresse
     * @param location Objet contenant les coordonnées de localisation
     */
    private void getGPSAddress(Location location) {

        final Location final_location = location;
        // Cette partie permet de compléter automatiquement le champ "addresse"
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            EditText address = findViewById(R.id.address);
            address.setText(addresses.get(0).getAddressLine(0));
        } catch (IOException e) { // Si on passe par ici, c'est qu'on a pas pu récupérer l'addresse
            e.printStackTrace();
            DialogManager.showDescriptionDialog(
                    getString(R.string.get_address_failed),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    getGPSAddress(final_location);
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;
                            }
                        }
                    }, _this);

        }
    }

    /**
     * Permet de gérer les permissions
     * @param requestCode Le code du groupe de permissions demandées
     * @param permissions Le tableau de permissions dans l'ordre où elle ont été demandées
     * @param grantResults La valeur de la permission autorisée ou non (index correspondant à l'index du tableau @permissions)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case GPS_PERMISSIONS:
                if (checkAllGpsAutorizations()) {
                    // La permission est garantie
                    Toast.makeText(this, getString(R.string.gps_accepte), Toast.LENGTH_SHORT).show();
                    getLocation();
                } else {
                    // La permission est refusée
                    Toast.makeText(this, getString(R.string.gps_refuse), Toast.LENGTH_SHORT).show();
                }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 10) { // Cela permet d'attendre que l'activité de configuration soit terminée avant de lancer la procédure de localisation
            getLocation();
        }
    }
}
