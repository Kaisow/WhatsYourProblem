package fr.lille1.maxime.whatsyourproblem.manager;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;

import fr.lille1.maxime.whatsyourproblem.R;

/**
 * Created by maxime on 10/12/2017.
 * Permet d'afficher de manière simple une snackbar
 */

public class SnackbarManager {
    public static void showSnackbar(Context context, CoordinatorLayout layout, String message, String okMessage, int duration) {
        Snackbar mySnackbar = Snackbar.make(layout, message, duration)
                .setAction(okMessage, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
        View snackbarView = mySnackbar.getView();
        snackbarView.setBackgroundColor(context.getColor(R.color.colorAccent));
        mySnackbar.setActionTextColor(context.getColor(R.color.white));
        mySnackbar.show();

    }
}
