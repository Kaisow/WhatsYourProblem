package fr.lille1.maxime.whatsyourproblem.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import fr.lille1.maxime.whatsyourproblem.database.MySQLiteHelper;
import fr.lille1.maxime.whatsyourproblem.model.Problem;

/**
 * Created by maxime on 26/11/2017.
 * Cette classe représente le pont entre un problème et la base de données.
 * C'est elle qui gère toutes les requêtes
 */
public class ProblemDAO {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = {
            MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_PROBLEM_TYPE,
            MySQLiteHelper.COLUMN_LATITUDE,
            MySQLiteHelper.COLUMN_LONGITUDE,
            MySQLiteHelper.COLUMN_ADDRESS,
            MySQLiteHelper.COLUMN_DESCRIPTION
    };

    public ProblemDAO(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    // Permet de gérer l'ouverture de la base de données
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    // Permet de gérer la fermeture de la base de données
    public void close() {
        dbHelper.close();
    }

    /**
     * Cette fonction gère l'ajout d'un nouveau problème dans la base
     * @param problem Le problème à ajouter
     * @return Le problème ajouté avec succès, null sinon
     */
    public Problem addProblem(Problem problem) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_PROBLEM_TYPE, problem.getProblemType());
        values.put(MySQLiteHelper.COLUMN_LATITUDE, problem.getLatitude());
        values.put(MySQLiteHelper.COLUMN_LONGITUDE, problem.getLongitude());
        values.put(MySQLiteHelper.COLUMN_ADDRESS, problem.getAddress());
        values.put(MySQLiteHelper.COLUMN_DESCRIPTION, problem.getDescription());

        long insertId = database.insert(MySQLiteHelper.TABLE_PROBLEM, null, values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_PROBLEM,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);

        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            return cursorToProblem(cursor);
        }
        return null;
    }

    /**
     * Cette fonction gère la génération des faux problèmes
     */
    public void addFakeProblem() {
        List<Problem> fakeProblems = new FakeProblems().getFakeList();

        open();

        for(Problem p : fakeProblems) {
            ContentValues values = new ContentValues();
            values.put(MySQLiteHelper.COLUMN_PROBLEM_TYPE, p.getProblemType());
            values.put(MySQLiteHelper.COLUMN_LATITUDE, p.getLatitude());
            values.put(MySQLiteHelper.COLUMN_LONGITUDE, p.getLongitude());
            values.put(MySQLiteHelper.COLUMN_ADDRESS, p.getAddress());
            values.put(MySQLiteHelper.COLUMN_DESCRIPTION, p.getDescription());

            long insertId = database.insert(MySQLiteHelper.TABLE_PROBLEM, null, values);
            if(insertId < 0) {
                close();
                return;
            }
        }
        close();
    }

    /**
     * Permet de récupérer l'ensemble des problèmes
     * @return L'ensemble des problème récupérés
     */
    public List<Problem> getAllProblems() {
        List<Problem> problems = new ArrayList<>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_PROBLEM,
                allColumns, null, null, null, null, null);

        if(cursor == null) return null;

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Problem problem = cursorToProblem(cursor);
            problems.add(problem);
            cursor.moveToNext();
        }
        cursor.close();
        return problems;
    }

    /**
     * Permet de récupérer un problème avec son id
     * @param id l'id du problème à récupérer
     * @return Le problème s'il a été trouvé, null sinon
     */
    public Problem getProblemById(long id) {
        Problem problem;

        Cursor cursor = database.query(MySQLiteHelper.TABLE_PROBLEM, allColumns, "_id = ?", new String[] {id + ""}, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            problem = cursorToProblem(cursor);
            return problem;
        }

        return null;
    }

    /**
     * Permet de vider totalement la table 'problem'
     */
    public void purgeAllProblem() {
        open();
        database.execSQL("DELETE FROM " + MySQLiteHelper.TABLE_PROBLEM);
        close();
    }

    /**
     * Permet de supprimer un problème avec son id
     * @param id l'id du problème à supprimer
     */
    public void removeProblemById(long id) {
        open();
        database.execSQL("DELETE FROM " + MySQLiteHelper.TABLE_PROBLEM + " WHERE _id = " + id);
        close();
    }

    /**
     * Facilite la récupération d'un problème à partir du curseur
     * @param cursor le curseur pointant sur le problème désiré
     * @return Le problème généré à partir du curseur
     */
    private Problem cursorToProblem(Cursor cursor) {
        Problem problem = new Problem();
        problem.setId(cursor.getLong(0));
        problem.setProblemType(cursor.getInt(1));
        problem.setLatitude(cursor.getString(2));
        problem.setLongitude(cursor.getString(3));
        problem.setAddress(cursor.getString(4));
        problem.setDescription(cursor.getString(5));
        return problem;
    }

}
