package fr.lille1.maxime.whatsyourproblem.model;

import fr.lille1.maxime.whatsyourproblem.R;

/**
 * Created by maxime on 26/11/2017.
 * Cette énumération gère tout les types de problèmes possibles
 * On utilise des id de string comme valeur exploitable pour
 * gérer les traductions
 */

public enum ProblemType {
    ARBRE_TAILLER (R.string.arbre_tailler),
    ARBRE_ABBATRE (R.string.arbre_abattre),
    DETRITUS (R.string.detritus),
    HAIE_TAILLER (R.string.haie_tailler),
    MAUVAISE_HERBE (R.string.mauvaise_herbe),
    AUTRE (R.string.autre);

    private int id;

    ProblemType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}