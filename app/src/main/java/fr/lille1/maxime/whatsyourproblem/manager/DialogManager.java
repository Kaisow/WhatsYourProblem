package fr.lille1.maxime.whatsyourproblem.manager;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by maxime on 30/11/2017.
 * Cette classe permet de gérer les boîtes de dialogue
 * Cela permet également d'éviter la répétition de code
 */

public class DialogManager {

    /**
     * Simplification pour l'affichage d'une boite de dialogue
     * @param message Le message à afficher
     * @param okListener Le listener permettant de savoir quelle action a été choisie ('OK' ou 'annuler')
     */
    public static void showDescriptionDialog(String message, DialogInterface.OnClickListener okListener, Context context) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
}
