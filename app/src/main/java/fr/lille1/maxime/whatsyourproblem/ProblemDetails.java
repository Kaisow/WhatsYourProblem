package fr.lille1.maxime.whatsyourproblem;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import fr.lille1.maxime.whatsyourproblem.dao.ProblemDAO;
import fr.lille1.maxime.whatsyourproblem.manager.DialogManager;
import fr.lille1.maxime.whatsyourproblem.model.Problem;

/**
 * Activité utilisée pour afficher le détail des problèmes
 */
public class ProblemDetails extends AppCompatActivity {

    // Permet d'accéder facilement au contexte dans les classes anonymes
    private final Context _this = this;

    // Permet de gérer le problème actuellement concerné pour les détails
    private Problem problem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problem_details);

        final ProblemDAO problemDAO = new ProblemDAO(this);

        // On récupère l'id du problème demandé
        final Long problem_id = getIntent().getLongExtra("problem_id", -1);

        // Si l'id est correcte, on fait une requête pour récupérer le problème
        if(problem_id > 0) {
            problemDAO.open();
            problem = problemDAO.getProblemById(problem_id);
            problemDAO.close();
        }

        // Si le problème est bien récupéré, on affecte ses valeurs dans les champs correspondants
        if(problem != null) {

            // On récupère tous les textes
            TextView detail_problem_type = findViewById(R.id.detail_problem_type);
            TextView detail_latitude = findViewById(R.id.detail_latitude);
            TextView detail_longitude = findViewById(R.id.detail_longitude);
            TextView detail_description = findViewById(R.id.detail_description);
            TextView detail_address = findViewById(R.id.detail_address);

            // On défini les infos récupérées de chaque problème
            detail_problem_type.setText(problem.getProblemType());
            detail_latitude.setText(problem.getLatitude());
            detail_longitude.setText(problem.getLongitude());
            detail_description.setText(problem.getDescription());
            detail_address.setText(problem.getAddress());

            // On gère le boutton de suppression
            Button delete_problem_button = findViewById(R.id.delete_problem_button);

            delete_problem_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Permet de gérer la confirmation de suppression
                    DialogManager.showDescriptionDialog(
                    getString(R.string.supprimer_probleme_confirmation),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    // L'utilisateur à confirmé, on peut supprimer
                                    problemDAO.removeProblemById(problem_id);
                                    // On indique que l'action s'est bien déroulée
                                    Intent intent = new Intent();
                                    intent.putExtra("last_action", "delete");
                                    setResult(Activity.RESULT_OK, intent);
                                    // On ferme l'activité
                                    finish();
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    Toast.makeText(_this, "Suppression annulée", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }
                    }, _this);
                }
            });

            // On gère le bouton de demande d'affichage de la map avec les coordonnées de l'emplacement du problème
            Button intent_navigation = findViewById(R.id.intent_navigation);
            intent_navigation.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.com/maps/search/?api=1&query=" + problem.getLatitude() + "," + problem.getLongitude()));
                    startActivity(intent);
                }
            });
        }
    }
}
